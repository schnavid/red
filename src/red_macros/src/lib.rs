extern crate proc_macro;
extern crate syn;
extern crate quote;

use proc_macro::TokenStream;
use proc_macro_hack::proc_macro_hack;
use quote::quote;
use syn::parse::{Parse, ParseStream, Result};
use syn::{FnArg, Expr, punctuated::Punctuated, parenthesized, Token, parse_macro_input, ExprClosure, ReturnType, Pat, PatType};

struct LispFn {
    params: Punctuated<FnArg, Token![,]>,
    body: Expr,
}

impl Parse for LispFn {
    fn parse(input: ParseStream) -> Result<Self> {
        let params;
        parenthesized!(params in input);
        let params = params.parse_terminated(FnArg::parse)?;
        Ok(LispFn {
            params,
            body: input.parse()?,
        })
    }
}

#[proc_macro_hack]
pub fn lisp_fn(input: TokenStream) -> TokenStream {
    let LispFn {
        params,
        body
    } = parse_macro_input!(input as LispFn);

    let arity = params.len();

//    LispFunction {
//        arity: FunctionArity::Exact(2),
//        function: |args| {
//            let res = (|x: i64, y: i64| x + y)(
//                Option::<i64>::from(args[0].clone()).ok_or(EvalError::WrongType)?,
//                Option::<i64>::from(args[1].clone()).ok_or(EvalError::WrongType)?,
//            );
//
//            Ok(res.into())
//        },
//    }

    let closure = ExprClosure {
        attrs: vec![],
        asyncness: None,
        movability: None,
        capture: None,
        or1_token: <_>::default(),
        inputs: {
            let mut p = Punctuated::new();

            for (i, param) in params.iter().enumerate() {
                if let FnArg::Typed(param) = param {
                    let param = param.clone();
                    p.push_value(Pat::Type(PatType {
                        attrs: param.attrs,
                        pat: param.pat,
                        colon_token: param.colon_token,
                        ty: param.ty,
                    }));
                    if i != params.len() - 1 {
                        p.push_punct(<_>::default());
                    }
                }
            }

            p
        },
        or2_token: <_>::default(),
        output: ReturnType::Default,
        body: Box::new(body),
    };

    let mut args = Punctuated::<Expr, Token![,]>::new();
    for (i, param) in params.iter().enumerate() {
        if let FnArg::Typed(param) = param {
            let ty = &param.ty;
            args.push_value((quote! { Option::<#ty>::from(args[#i].clone()).ok_or(crate::lisp::eval::EvalError::WrongType)? }).into())
        }

        if i != params.len() - 1 {
            args.push_punct(<_>::default());
        }
    }

    (quote! { crate::lisp::functions::LispFunction::new(
        |args| {
            let res = (#closure)(#args);

            Ok(res.into())
        },
        crate::lisp::FunctionArity::Exact(#arity),
    ) }).into()
}

//fn impl_hello_macro(ast: &syn::DeriveInput) -> TokenStream {
//    let name = &ast.ident;
//    let gen = quote! {
//        impl HelloMacro for #name {
//            fn hello_macro() {
//                println!("Hello, Macro! My name is {}", stringify!(#name));
//            }
//        }
//    };
//    gen.into()
//}