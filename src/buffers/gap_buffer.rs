use crate::buffers::{MutableBuffer, Buffer};
use std::fs::File;
use std::io::{Error, Read};
use std::ops::Index;
use std::cmp::Ordering;
use std::collections::VecDeque;

/// Text Buffer implemented using a `VecDeque` as the internal buffer
pub struct GapBuffer {
    offset: usize,
    buf: VecDeque<u8>,
}

impl GapBuffer {
    fn with_capacity(n: usize) -> GapBuffer {
        GapBuffer {
            offset: 0,
            buf: VecDeque::with_capacity(n),
        }
    }

    fn get_index(&self, i: usize) -> usize {
        if i < self.offset {
            (self.buf.len() - self.offset) + i
        } else if i < self.buf.len() {
            i - self.offset
        } else {
            i
        }
    }

    fn shift(&mut self, to: usize) {
        match to.cmp(&self.offset) {
            Ordering::Equal => return,
            Ordering::Less => {
                let mut last = self.buf.pop_back().unwrap();
                self.offset -= 1;
                while to < self.offset {
                    self.buf.push_front(last);
                    last = self.buf.pop_back().unwrap();
                    self.offset -= 1;
                }
                self.buf.push_front(last);
            }
            Ordering::Greater => {
                let mut first = self.buf.pop_front().unwrap();
                self.offset += 1;
                while to > self.offset {
                    self.buf.push_back(first);
                    first = self.buf.pop_front().unwrap();
                    self.offset += 1;
                }
                self.buf.push_back(first);
            }
        }
    }

    fn insert_data(&mut self, at: usize, data: &str) {
        assert!(at <= self.buf.len(), "Index out out of bounds!");

        self.shift(at);

        let data = data.as_bytes();

        self.buf.reserve(data.len());
        for v in data {
            self.offset += 1;
            self.buf.push_back(*v);
        }
    }

    fn remove_data(&mut self, from: usize, to: usize) {
        if self.buf.len() <= from || self.buf.len() <= to {
            return;
        }

        self.shift(from);

        for _ in from..to + 1 {
            if let None = self.buf.pop_front() { break; }
        }
    }

    fn items(&self) -> Items {
        Items(self, 0)
    }
}

impl Index<usize> for GapBuffer {
    type Output = u8;

    fn index(&self, index: usize) -> &Self::Output {
        let index = self.get_index(index);
        &self.buf[index]
    }
}

struct Items<'a>(&'a GapBuffer, usize);

impl<'a> Items<'a> {
    fn lines(mut self) -> Box<[String]> {
        let mut buf = Vec::new();

        let len = self.0.buf.len();
        while self.1 < len {
            let mut str_buf = Vec::with_capacity(80);

            while self.1 < len && self.0[self.1] != b'\n' {
                str_buf.push(self.0[self.1]);
                self.1 += 1;
            }

            if self.1 < len { self.1 += 1; }

            buf.push(String::from_utf8(str_buf).unwrap());
        }

        buf.into_boxed_slice()
    }
}

impl<'a> Iterator for Items<'a> {
    type Item = u8;

    fn next(&mut self) -> Option<Self::Item> {
        if self.1 >= self.0.buf.len() {
            None
        } else {
            let x = self.0.buf.get(self.0.get_index(self.1)).cloned();
            self.1 += 1;
            x
        }
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        let len = self.0.buf.len();
        (len - self.1, Some(len - self.1))
    }
}

impl<'a> Read for Items<'a> {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize, Error> {
        let mut i = 0;

        while i < buf.len() {
            if let Some(x) = self.next() {
                buf[i] = x;
                i += 1;
            } else {
                break;
            }
        }

        Ok(i)
    }
}

impl Buffer for GapBuffer {
    fn get_lines(&self, from: usize, to: usize) -> Box<[String]> {
        let mut buf = Vec::with_capacity(to - from);
        let lines = self.items().lines();

        for i in from..to + 1 {
            if i >= lines.len() { break; }

            buf.push(lines.get(i).cloned().unwrap_or("".to_string()));
        }

        buf.into_boxed_slice()
    }

    fn from_file(file: File) -> Result<Self, Error> {
        let mut file = file;

        let size = file.metadata().unwrap().len() as usize;

        let mut g = GapBuffer::with_capacity(size);

        let mut buf = Vec::with_capacity(size);

        file.read_to_end(&mut buf).unwrap();

        g.insert_data(0, String::from_utf8(buf).unwrap().as_str());

        Ok(g)
    }

    fn from_text(text: &str) -> Self {
        let size = text.len();

        let mut g = GapBuffer::with_capacity(size);

        g.insert_data(0, text);

        g.shift(0);

        g
    }
}

impl MutableBuffer for GapBuffer {
    fn insert(&mut self, at: usize, data: &str) {
        self.insert_data(at, data);
    }

    fn remove(&mut self, from: usize, to: usize) {
        self.remove_data(from, to);
    }
}