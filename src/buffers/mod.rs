use std::fs::File;

/// A text buffer
pub trait Buffer: Sized {
    /// Get certain lines (separated by `\n` for now) from the buffer from `from` until inclusive `to`
    fn get_lines(&self, from: usize, to: usize) -> Box<[String]>;
    /// Create buffer with the contents from a file
    fn from_file(file: File) -> std::io::Result<Self>;
    /// Create buffer with the contents from the given `text`
    fn from_text(text: &str) -> Self;
}

/// A mutable Buffer, i.e. in which data can be mutated (inserted and removed)
pub trait MutableBuffer: Buffer {
    /// Insert `data` into buffer at index `at`
    fn insert(&mut self, at: usize, data: &str);
    /// Remove data in buffer from index `from` until inclusive index `to`
    fn remove(&mut self, from: usize, to: usize);
}

/// [Gap Buffer](https://en.wikipedia.org/wiki/Gap_buffer) implementation
mod gap_buffer;
pub use gap_buffer::GapBuffer;