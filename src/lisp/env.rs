//! Lisp Environment

use crate::lisp::object::LispObject;
use std::collections::HashMap;
use crate::lisp::numbers;

/// Lisp runtime environment
pub struct LispEnvironment {
    scopes: Vec<Scope>,
    current: usize,
}

#[derive(Clone)]
struct Scope {
    definitions: HashMap<String, LispObject>,
}

impl Scope {
    fn new() -> Scope {
        Scope {
            definitions: HashMap::new()
        }
    }

    fn lookup(&self, s: &String) -> Option<LispObject> {
        self.definitions.get(s).cloned()
    }

    fn define(&mut self, name: String, obj: LispObject) {
        self.definitions.insert(name, obj);
    }
}

impl LispEnvironment {
    /// Create a new `LispEnvironment`
    pub fn new() -> LispEnvironment {
        let mut e = LispEnvironment {
            scopes: vec![Scope::new()],
            current: 0
        };

        e.define("define".to_string(), LispObject::SpecialDefine);
        e.define("lambda".to_string(), LispObject::SpecialLambda);
        e.define("macro".to_string(), LispObject::SpecialMacro);

        numbers::register(&mut e);

        e
    }

    /// Look for a symbol and return the object it points to or `#nil` if it is not defined
    pub fn lookup(&self, s: &String) -> LispObject {
        for i in (0..=self.current).rev() {
            if let Some(obj) = self.scopes[i].lookup(s) {
                return obj;
            }
        }

        LispObject::Nil
    }

    /// Push a new empty scope onto the stack
    pub fn new_scope(&mut self) {
        self.scopes.push(Scope::new());
        self.current += 1;
    }

    /// Pop the current scope from the stack
    pub fn pop_scope(&mut self) {
        if self.current > 0 {
            self.current -= 1;
            self.scopes.pop();
        }
    }

    /// Define a symbol in the current scope
    pub fn define(&mut self, name: String, obj: LispObject) {
        self.scopes[self.current].define(name, obj);
    }

    /// Define a symbol in the global scope
    pub fn define_global(&mut self, name: String, obj: LispObject) {
        self.scopes[0].define(name, obj);
    }
}