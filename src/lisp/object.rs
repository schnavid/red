//! Lisp Objects

use std::fmt::{Display, Formatter};
use crate::lisp::functions::LispFunction;

/// Value in Lisp Code
#[derive(Clone)]
pub enum LispObject {
    /// Integer
    Integer(i64),
    /// Float
    Float(f64),
    /// Symbol
    Symbol(String),
    /// String
    String(String),
    /// List
    List(Vec<LispObject>),
    ///
    Quote(Box<LispObject>),
    /// Bool
    Bool(bool),
    /// Nil
    Nil,
    /// Function
    Function(Vec<String>, Vec<LispObject>),
    /// Native Function
    NativeFunction(LispFunction),
    ///
    SpecialDefine,
    ///
    SpecialLambda,
    ///
    SpecialMacro,
}

impl Display for LispObject {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        match self {
            LispObject::Integer(v) => write!(f, "{}", v),
            LispObject::Float(v) => write!(f, "{}", v),
            LispObject::Symbol(v) => write!(f, "{}", v),
            LispObject::String(v) => write!(f, "\"{}\"", v),
            LispObject::List(v) => {
                write!(f, "( ")?;
                for x in v {
                    write!(f, "{} ", x)?
                }
                write!(f, ")")
            }
            LispObject::Quote(v) => write!(f, "'{}", v),
            LispObject::Bool(v) => write!(f, "{}", if *v { "#t" } else { "#f" }),
            LispObject::Nil => write!(f, "#nil"),
            LispObject::Function(args, bodies) => {
                write!(f, "#function< ")?;
                for x in args {
                    write!(f, "{} ", x)?
                }
                write!(f, "><")?;
                for x in bodies {
                    write!(f, "{} ", x)?
                }
                write!(f, ">")
            }
            LispObject::NativeFunction(_) => write!(f, "#native_fn"),
            LispObject::SpecialDefine => write!(f, "#define"),
            LispObject::SpecialLambda => write!(f, "#lambda"),
            LispObject::SpecialMacro => write!(f, "#macro"),
        }
    }
}

impl LispObject {
    /// Get type name of object
    pub fn get_type(&self) -> String {
        match self {
            LispObject::Integer(_) => "Integer".to_string(),
            LispObject::Float(_) => "Float".to_string(),
            LispObject::Symbol(_) => "Symbol".to_string(),
            LispObject::String(_) => "String".to_string(),
            LispObject::List(_) => "List".to_string(),
            LispObject::Quote(_) => "ERR".to_string(),
            LispObject::Bool(_) => "Boolean".to_string(),
            LispObject::Nil => "Nil".to_string(),
            LispObject::Function(_, _) => "Function".to_string(),
            LispObject::NativeFunction(_) => "Function".to_string(),
            LispObject::SpecialDefine => "ERR".to_string(),
            LispObject::SpecialLambda => "ERR".to_string(),
            LispObject::SpecialMacro => "ERR".to_string(),
        }
    }

    /// Return if `self` is a bool
    pub fn is_bool(&self) -> bool {
        match self {
            LispObject::Bool(_) => true,
            _ => false
        }
    }

    /// Return if `self` is a string
    pub fn is_string(&self) -> bool {
        match self {
            LispObject::String(_) => true,
            _ => false
        }
    }

    /// Return if `self` is a list
    pub fn is_list(&self) -> bool {
        match self {
            LispObject::List(_) => true,
            _ => false
        }
    }

    /// Return if `self` is a function
    pub fn is_function(&self) -> bool {
        match self {
            LispObject::Function(_, _) => true,
            LispObject::NativeFunction(_) => true,
            _ => false
        }
    }

    /// Return if `self` is a symbol
    pub fn is_symbol(&self) -> bool {
        match self {
            LispObject::Symbol(_) => true,
            _ => false
        }
    }

    /// Return symbol if `self` is a symbol
    pub fn as_symbol(&self) -> Option<String> {
        match self {
            LispObject::Symbol(s) => Some(s.clone()),
            _ => None
        }
    }
}

impl From<i64> for LispObject {
    fn from(v: i64) -> Self {
        LispObject::Integer(v)
    }
}

impl From<f64> for LispObject {
    fn from(v: f64) -> Self {
        LispObject::Float(v)
    }
}

impl From<String> for LispObject {
    fn from(v: String) -> Self {
        LispObject::String(v)
    }
}

impl From<bool> for LispObject {
    fn from(v: bool) -> Self {
        LispObject::Bool(v)
    }
}

impl From<Vec<LispObject>> for LispObject {
    fn from(v: Vec<LispObject>) -> Self {
        LispObject::List(v)
    }
}

/// Errors that can occur during Parsing
#[derive(Debug, PartialEq)]
pub enum ParseError {
    /// Unexpected Character
    UnexpectedCharacter(char),
    /// Reached End of File unexpectedly (e.g. missing closing `)`)
    EndOfFileReached,
    /// Invalid Float Literal (e.g. `1.0.0`)
    InvalidFloatLiteral,
    /// Invalid Integer Literal
    InvalidIntegerLiteral,
}

//impl Error for ParseError {}

/// Parses `input` into a list of `LispAst`
///
/// # Examples
///
/// ```
/// # use red::lisp::ast::{read, LispAst};
///
/// let x = read("( 1 3 )");
///
/// assert_eq!(x, Ok(vec![LispAst::List(vec![LispAst::Integer(1), LispAst::Integer(3)])]));
/// ```
pub fn read(input: &str) -> Result<Vec<LispObject>, ParseError> {
    let mut result = Vec::new();

    let input = input.chars().collect::<Vec<char>>();

    let mut i = 0;
    let len = input.len();

    fn parse_value(i: &mut usize, input: &Vec<char>) -> Result<LispObject, ParseError> {
        match input[*i] {
            '(' => {
                let mut items = Vec::new();

                *i += 1;

                loop {
                    while *i < input.len() && input[*i].is_whitespace() { *i += 1; }

                    if *i < input.len() {
                        if input[*i] == ')' {
                            *i += 1;
                            return Ok(LispObject::List(items));
                        } else {
                            items.push(parse_value(i, input)?);
                        }
                    } else {
                        return Err(ParseError::EndOfFileReached);
                    }
                }
            }
            '0'..='9' => {
                let mut result = String::new();

                while *i < input.len() && (input[*i].is_numeric() || input[*i] == '.') {
                    result.push(input[*i]);
                    *i += 1;
                }

                if result.contains('.') {
                    Ok(LispObject::Float(result.parse::<f64>().map_err(|_| ParseError::InvalidFloatLiteral)?))
                } else {
                    Ok(LispObject::Integer(result.parse::<i64>().map_err(|_| ParseError::InvalidIntegerLiteral)?))
                }
            }
            '\'' => {
                *i += 1;

                while *i < input.len() && input[*i].is_whitespace() { *i += 1; }

                Ok(LispObject::Quote(Box::new(parse_value(i, input)?)))
            }
            _ => {
                let forbidden = "'\"()[]{};#";

                if forbidden.contains(input[*i]) {
                    Err(ParseError::UnexpectedCharacter(input[*i]))
                } else {
                    let mut result = String::new();

                    while *i < input.len() && (!input[*i].is_whitespace() && !forbidden.contains(input[*i])) {
                        result.push(input[*i]);
                        *i += 1;
                    }

                    Ok(LispObject::Symbol(result))
                }
            }
        }
    }

    while i < len {
        while i < len && input[i].is_whitespace() { i += 1; }

        result.push(parse_value(&mut i, &input)?);
    }

    Ok(result)
}