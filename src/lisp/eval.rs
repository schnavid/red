//! Lisp Evaluation

use crate::lisp::object::LispObject;
use crate::lisp::env::LispEnvironment;

/// Errors that can occur during evaluation
#[derive(Debug)]
pub enum EvalError {
    /// Tried to call a non-function
    NotAFunction,
    /// Given number of arguments is not valid for the given function
    WrongNumberOfArguments,
    /// Expected a different type
    WrongType,
}

/// Evaluate given object
pub fn eval(obj: &LispObject, env: &mut LispEnvironment) -> Result<LispObject, EvalError> {
    match obj {
        LispObject::Symbol(v) => Ok(env.lookup(v)),
        LispObject::List(v) => match v.as_slice() {
            [] => Ok(obj.clone()),
            _ => {
                let func = &v[0];

                match eval(func, env)? {
                    LispObject::Function(params, bodies) => {
                        let args = &v[1..];

                        if params.len() != args.len() {
                            Err(EvalError::WrongNumberOfArguments)
                        } else {
                            env.new_scope();

                            let args_res = args.iter().map(|x| eval(x, env));
                            let mut args = Vec::with_capacity(args_res.len());
                            for arg in args_res {
                                args.push(arg?);
                            }

                            for i in 0..args.len() {
                                env.define(params[i].clone(), args[i].clone());
                            }

                            let mut result = LispObject::Nil;

                            for body in bodies {
                                result = eval(&body, env)?;
                            }

                            env.pop_scope();

                            Ok(result)
                        }
                    }
                    LispObject::NativeFunction(f) => {
                        let args = &v[1..];

                        if !f.arity().acceptable(args.len()) {
                            return Err(EvalError::WrongNumberOfArguments);
                        }

                        let args_res = args.iter().map(|x| eval(x, env));
                        let mut args = Vec::with_capacity(args_res.len());
                        for arg in args_res {
                            args.push(arg?);
                        }

                        f.call(args.as_slice())
                    }
                    LispObject::SpecialDefine => {
                        let args = &v[1..];

                        if 2 != args.len() {
                            Err(EvalError::WrongNumberOfArguments)
                        } else {
                            match args[0].as_symbol() {
                                Some(s) => {
                                    let arg = eval(&args[1], env);
                                    env.define(s, arg?);
                                    Ok(LispObject::Nil)
                                }
                                None => Err(EvalError::WrongType)
                            }
                        }
                    }
                    LispObject::SpecialLambda => {
                        let args = &v[1..];

                        if args.len() < 2 {
                            Err(EvalError::WrongNumberOfArguments)
                        } else {
                            match &args[0] {
                                LispObject::List(v) => {
                                    let mut params = Vec::with_capacity(v.len());

                                    for param in v {
                                        params.push(param.as_symbol().ok_or(EvalError::WrongType)?);
                                    }

                                    Ok(LispObject::Function(params, args[1..].to_vec()))
                                }
                                LispObject::Symbol(s) => Ok(LispObject::Function(vec![s.clone()], args[1..].to_vec())),
                                _ => Err(EvalError::WrongType)
                            }
                        }
                    }
                    _ => Err(EvalError::NotAFunction)
                }
            }
        }
        LispObject::Quote(obj) => Ok(*obj.clone()),
        _ => Ok(obj.clone())
    }
}