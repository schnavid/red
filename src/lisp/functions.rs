//! Lisp Functions

use crate::lisp::object::LispObject;
use crate::lisp::FunctionArity;
use crate::lisp::eval::EvalError;

/// Native Function
pub type NativeFunction = fn(&[LispObject]) -> Result<LispObject, EvalError>;

/// Wrapper for Native Functions
#[derive(Clone)]
pub struct LispFunction {
    function: NativeFunction,
    arity: FunctionArity,
}

impl LispFunction {
    /// Create a new LispFunction
    pub fn new(function: NativeFunction, arity: FunctionArity) -> LispFunction {
        LispFunction {
            function,
            arity
        }
    }

    /// Get the arity of the inner function
    pub fn arity(&self) -> FunctionArity {
        self.arity
    }

    /// Call the inner function with the given arguments
    pub fn call(&self, args: &[LispObject]) -> Result<LispObject, EvalError> {
        (self.function)(args)
    }
}