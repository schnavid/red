//! Lisp Numbers

use crate::lisp::object::LispObject;
use crate::lisp::env::LispEnvironment;

impl LispObject {
    /// Return if `self` is an integer
    pub fn is_integer(&self) -> bool {
        match self {
            LispObject::Integer(_) => true,
            _ => false
        }
    }

    /// Return integer if `self` is an integer
    pub fn as_integer(&self) -> Option<i64> {
        self.clone().into()
    }

    /// Return integer
    ///
    /// # Panics
    ///
    /// Panics if `self` is not an integer
    pub fn as_integer_or_error(&self) -> i64 {
        self.clone().into()
    }

    /// Return if `self` is a float
    pub fn is_float(&self) -> bool {
        match self {
            LispObject::Float(_) => true,
            _ => false
        }
    }

    /// Return float if `self` is a float
    pub fn as_float(&self) -> Option<f64> {
        self.clone().into()
    }

    /// Return Float
    ///
    /// # Panics
    ///
    /// Panics if `self` is not a float
    pub fn as_float_or_error(&self) -> f64 {
        self.clone().into()
    }
}

impl From<LispObject> for Option<i64> {
    fn from(v: LispObject) -> Self {
        match v {
            LispObject::Integer(v) => Some(v),
            _ => None
        }
    }
}

impl From<LispObject> for i64 {
    fn from(v: LispObject) -> Self {
        let t = v.get_type();
        Option::<i64>::from(v).unwrap_or_else(|| panic!("Wrong type: expected: Integer, found: {}", t))
    }
}

impl From<LispObject> for Option<f64> {
    fn from(v: LispObject) -> Self {
        match v {
            LispObject::Float(v) => Some(v),
            _ => None
        }
    }
}

impl From<LispObject> for f64 {
    fn from(v: LispObject) -> Self {
        let t = v.get_type();
        Option::<f64>::from(v).unwrap_or_else(|| panic!("Wrong type: expected: Float, found: {}", t))
    }
}

/// A general way of working with numbers whether integer or float
#[derive(Clone, Copy)]
pub enum LispNumber {
    /// Integer
    Integer(i64),
    /// Floating Point Number
    Float(f64),
}

impl From<LispObject> for Option<LispNumber> {
    fn from(obj: LispObject) -> Self {
        match obj {
            LispObject::Integer(v) => Some(LispNumber::Integer(v)),
            LispObject::Float(v) => Some(LispNumber::Float(v)),
            _ => None
        }
    }
}

impl From<LispObject> for LispNumber {
    fn from(v: LispObject) -> Self {
        let t = v.get_type();
        Option::<LispNumber>::from(v).unwrap_or_else(|| panic!("Wrong type: expected: Number, found: {}", t))
    }
}

impl LispObject {
    /// Return if `self` is a number
    pub fn is_number(&self) -> bool {
        self.is_integer() || self.is_float()
    }

    /// Return LispNumber if `self` is a number
    pub fn as_number(&self) -> Option<LispNumber> {
        self.clone().into()
    }

    /// Return LispNumber
    ///
    /// # Panics
    ///
    /// Panics if `self` is not a number
    pub fn as_number_or_error(&self) -> LispNumber {
        self.clone().into()
    }
}

/// Define everything from this module for the given `LispEnvironment`
pub fn register(env: &mut LispEnvironment) {
    env.define_global("number?".to_string(), LispObject::NativeFunction(lisp_fn!( (x: LispObject) x.is_number() )));
    env.define_global("integer?".to_string(), LispObject::NativeFunction(lisp_fn!( (x: LispObject) x.is_integer() )));
    env.define_global("float?".to_string(), LispObject::NativeFunction(lisp_fn!( (x: LispObject) x.is_float() )));
    env.define_global("number?".to_string(), LispObject::NativeFunction(lisp_fn!( (x: LispObject) x.is_number() )));
}