pub mod object;
pub mod eval;
pub mod env;
pub mod numbers;
pub mod functions;
pub mod lists;

/// Describes how many Arguments a native lisp function can take
#[derive(Clone, Copy)]
pub enum FunctionArity {
    /// The function takes an exact amount of arguments
    Exact(usize),
    /// The Function takes a minimum amount of arguments
    Minimum(usize),
}

impl FunctionArity {
    /// Checks if the given amount of arguments is valid for this function
    pub fn acceptable(&self, num: usize) -> bool {
        match self {
            FunctionArity::Exact(n) => *n == num,
            FunctionArity::Minimum(n) => *n <= num,
        }
    }
}