#![warn(missing_docs)]

//! # Rusty EDitor
//!
//! A (highly wip) text editor

extern crate glium;
extern crate linefeed;
extern crate red_macros;

use proc_macro_hack::proc_macro_hack;

/// Create a LispFunction
///
/// # Example
///
/// ```
/// # use crate::lisp::env::LispEnvironment;
/// # use crate::lisp::eval::eval;
/// # use crate::lisp::object::{LispObject, read, ParseError};
/// #
/// # fn main() -> Result<(), ParseError> {
/// let env = LispEnvironment::new();
/// env.define_global("number?".to_string(), LispObject::NativeFunction(lisp_fn!( (x: LispObject) x.is_number() )));
///
/// assert_eq!(eval(read("(number? 3)")?, &mut env), Ok(LispObject::Bool(true)));
/// # Ok(())
/// # }
/// ```
#[proc_macro_hack]
pub use red_macros::lisp_fn;

use crate::lisp::object::LispObject;
use std::sync::Arc;
use crate::lisp::env::LispEnvironment;
use linefeed::{ReadResult, Interface};
use crate::lisp::eval::eval;

/// Buffers for storing text
pub mod buffers;

/// Utilities
pub mod util;

/// Lisp
pub mod lisp;

fn main() -> std::io::Result<()> {
    use lisp::object::read;

    let interface = Arc::new(Interface::new("red")?);
    interface.set_prompt("repl> ")?;

    let mut env = LispEnvironment::new();

    env.define("identity".to_string(), LispObject::Function(vec!["x".to_string()], vec![LispObject::Symbol("x".to_string())]));
    env.define("+".to_string(), LispObject::NativeFunction(lisp_fn!( (x: i64, y: i64) x + y )));

    while let ReadResult::Input(line) = interface.read_line()? {
        match read(line.trim()) {
            Ok(v) => for x in v {
                match eval(&x, &mut env) {
                    Ok(o) => println!("{}", o),
                    Err(err) => println!("{:?}", err)
                }
            },
            Err(e) => println!("{:?}", e)
        }
    }

    println!("Goodbye.");

//    let file = File::open("test.txt").unwrap();
//
//    let _buffer: GapBuffer = GapBuffer::from_file(file).unwrap();
//
//    let event_loop = EventLoop::new();
//
//    let wb = WindowBuilder::new()
//        .with_title("red")
//        .with_inner_size(LogicalSize::new(1920.0, 1080.0));
//
//    let cb = ContextBuilder::new();
//
//    let display = Display::new(wb, cb, &event_loop).unwrap();
//
//    util::start_loop(event_loop, move |events| {
//        let mut action = Action::Continue;
//
//        for event in events {
//            action = match event {
//                Event::WindowEvent { event, .. } => match event {
//                    WindowEvent::CloseRequested => Action::Stop,
//                    _ => action,
//                },
//                _ => action,
//            }
//        }
//
//        action
//    });

    Ok(())
}
